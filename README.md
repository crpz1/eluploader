# eluploader
A not so good file uploader for use with the file host [elixi.re](https://elixi.re) that I finally bothered to upload after sitting on my computer for the past 6 or so months

This project was really just an excuse to learn how to write an Android app and thus, isn't that good and doesn't follow many proper development conventions
e.g. hard coded strings, lack of documentation, bad ui, etc.

Tested on a Xiaomi Redmi Note 5 on Android 9 and a Google Pixel 6 on Android 12