package club.ralsei_appreciation.eluploader;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface ElixireService {

    @Multipart
    @Headers("User-Agent: eluploader/" + BuildConfig.VERSION_NAME)
    @POST("upload")
    Call<UploadedFile> uploadFile(@Part MultipartBody.Part file, @Header("Authorization") String token);
}
